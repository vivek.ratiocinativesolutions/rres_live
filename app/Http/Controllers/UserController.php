<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function getUsers(Request $request)
    {
        $data = [];
        if($request->has('q')){
            $search = $request->q;
            $data = User::where(function($query) use ($search) {
                $query->where('name', 'like', "%$search%");
                $query->orWhere('last_name', 'like', "%$search%");
                $query->orWhere('email', 'like', "%$search%");
            })->get();
        } else {
            $data = User::where('id', '<>', 0)->limit(100)->get();
        }
        return response()->json($data);
    }
}
