@extends('layouts/contentLayoutMaster')

@section('title', 'Live - ' . $event->title)

@section('vendor-style')

    @include('inc/sweet-alert/styles')
    <!-- vendor css files -->
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-chat.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-chat-list.css')) }}">
    <style>
        body {
            background-color: #fff !important;
        }

        .vertical-layout.vertical-menu-modern.menu-collapsed .app-content,
        .vertical-layout.vertical-menu-modern.menu-collapsed .footer {
            /* padding-top: 13px !important; */
        }

        .agora_video_player {
            object-fit: contain !important;
            position: unset!important;
        }

    </style>
@endsection

@section('breadcrumb_right')

@endsection

@section('content')
    @if (auth()->user()->hasRole('admin') ||
        auth()->user()->hasRole('super_admin'))
        <div class="row mb-1 live-header align-items-center">
            <div class="col-md-3 col-8 order-md-1 order-1">
                <a class="navbar-brand pr-2" href="{{  url('/') }}">
                    <span class="brand-logo">
                        <img width="50" src="{{  asset("images/logo/logo.png")  }}" alt=""></span>
                    <div class="d-inline h4"><span class="text-dark">RESEARCHERS</span> <span
                            class="text-primary"><i>LIVE</i></span></div>
                </a>
            </div>
            <div class="col-md-7 col-12 order-md-2 order-3 px-0">
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a class='confirm-redirect' href='javascript:void();' data-href="{{ route('events.index') }}">
                                Events
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            {{ $event->title }}
                        </li>
                    </ol>
                </div>
            </div>
            <div class="col-md-2 col-4 order-md-3 order-2 text-right">
                <a class=' confirm-redirect' data-href="{{ route('events.index') }}">
                    <i data-feather="log-out"></i> Exit
                </a>
            </div>
        </div>
    @endif

    <div class="container">
        <div class="row live-body">
            <div class="col-md-7 col-12 ">
                <div class="flex flex-row " >
                    <div class="item">
                      <div class="row mb-2 align-items-center">
                        <div class="col-12">
                          <div class="d-inline-block text-left">
                              <div class='stream-controls d-none'>
                                  <button id="screen-share-btn" class='btn btn-outline-primary '>
                                      <i data-feather="airplay"></i> Screen Share
                                  </button>
      
                                  <button id="camera-btn" class='btn btn-outline-primary d-none'>
                                      <i data-feather="camera"></i> Camera
                                  </button>
      
      
                                  <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                      <button data-icon="camera" class="btn btn-outline-primary btn-icon toggle-camera">
                                          <i data-feather="camera"></i>
                                      </button>
      
                                      <div class="btn-group" role="group">
                                          <button type="button" class="btn btn-outline-primary dropdown-toggle px-0 pr-2"
                                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      
                                          </button>
                                          <div class="cam-list dropdown-menu"></div>
                                      </div>
                                  </div>
      
      
                                  <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                      <button data-icon="mic" class="btn btn-outline-primary btn-icon toggle-mic">
                                          <i data-feather="mic"></i>
                                      </button>
      
                                      <div class="btn-group" role="group">
                                          <button type="button" class="btn btn-outline-primary dropdown-toggle px-0 pr-2"
                                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          </button>
                                          <div class="mic-list dropdown-menu"></div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          
                          <span class=" mt-md-0 mt-1 float-md-right">
                              <button data-toggle='modal' data-target="#media-device-test"
                                  class="btn btn-outline-primary test-live">Cam & Mic Devices</button>
                              @if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('admin'))
                                  {{-- <span class="text-success mr-1">Ready to Stream</span> --}}
                                  <button data-status='live' class="btn btn-danger change-live-status">Go Live</button>
                              @endif
                          </span>
                          <span class="d-none you-live float-right mr-1">
                              <span class="btn btn-outline-success no-cursor">
                                <i data-feather='airplay'></i>
                                  You are presenting now
                              </span>
                            </span>
                        </div>
                      </div>
                    </div>
                    <div class="item">
                      <div class="row align-items-center video-feed-container shadow-sm" >
                        <div class="col-12 text-center" >
                            <div class="message-box d-flex justify-content-center  text-center flex-column" style="max-height: 700px;">
                                <div class="item message-box-text my-3">
                                    <h3>No active user in the stream.</h3>
                                </div>
                                <div class="item justify-self-end">
                                    {{-- <img class='img-fluid' src="{{  asset('images/app/video_player.png') }}" alt="Video Player"> --}}
                                    <img style="max-height: 400px; width: 500px; border-radius: 5px;" class='img-fluid' src="{{  $event->getImage() }}" alt="{{ $event->title }}">
                                </div>
                            </div>
                            <div id="local-player"  class="player h-100">
                            
                          </div>
                          <div class="progress live-progress-bar rounded-0 d-none">
                              <div class="progress-bar bg-success " role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                  aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-12 event-chat-sec mt-md-0 mt-2">
                
                <ul class="nav nav-tabs mb-2" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active pt-0" id="information-tab" data-toggle="tab" href="#information"
                            aria-controls="home" role="tab" aria-selected="true"> <i data-feather="info"></i> Event</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pt-0" id="chat-box-tab" data-toggle="tab" href="#chat-box" aria-controls="profile"
                            role="tab" aria-selected="false">
                            <i data-feather="message-square"></i>Chat</a>
                    </li>
                    <li class="nav-item ml-1 d-none">
                        <a class="nav-link pt-0" id="notes-box-tab" data-toggle="tab" href="#notes-box" aria-controls="profile"
                            role="tab" aria-selected="false">
                            <i data-feather="message-square"></i>Notes:</a>
                    </li>
                </ul>
    
                <div class="tab-content pt-75">
                    <div role="tabpanel" class="tab-pane active" id="information" aria-labelledby="chat-box"
                        aria-expanded="true">
    
                        <div class="row ">
                            {{-- <div class="col-12 mb-2">
                                <img class="img-fluid rounded event-image" src="{{ $event->getImage() }}"
                                    alt="{{ $event->title }}" id="logo-placeholder" />
                            </div> --}}
                            <div class="col-12">
                                <div class="alert alert-danger event-notice">
                                    <div class="alert-body ">
                                        Event is not LIVE yet.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12 mb-2">
                                <h4>Title</h4>
                                <p>
                                    {{ $event->title }}
                                </p>
                            </div>
                            <div class="col-md-6 col-12 mb-2">
                                <h4>Date & Time</h4>
                                <p>
                                    @displayDate($event->start_date_time, "d M Y | h:i A")
                                </p>
                            </div>
                            @if ($event->keywords)
                                <div class="col-12 mb-2">
                                    <h4>Keywords</h4>
                                    <p>
                                        @foreach ($event->keywords as $keyword)
                                            <span class="btn btn-light mr-50 mb-50 no-cursor">{{ $keyword->keyword_name }}</span>
                                        @endforeach
                                    </p>
                                </div>
                            @endif
                        </div>
                        <div class="row  ">
                            <div class="col-12">
                                <div class=' pb-1'>
                                    <h4>Event Organizer(s)</h4>
                                </div>
                                <div id="active-users" class="row ">
                                    <div class="col-md-3 col-6 text-center user-{{ $event->eventAdmin->id }}">
                                        <div class="avatar avatar-xl">
                                            <img class='img-fulid rounded-circle' src="{{ $event->eventAdmin->profileImage() }}"
                                                alt="{{ $event->eventAdmin->fullName() }}">
                                            {{-- <span class="avatar-status-online"></span> --}}
                                        </div>
                                        <p class="text-center">
                                            {{ $event->eventAdmin->fullName() }}
                                            <small class='d-block'>Admin of Event</small>
                                            <small class='text-danger user-status d-block mb-50'>Offline</small>
                                            @if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('admin'))
                                                <button data-user="{{ $event->eventAdmin->id }}"
                                                    class="btn  btn-block d-block btn-outline-danger enter-stream btn-sm">Add to Stream</button>
                                            @endif
                                        </p>
                                    </div>
                                    @foreach ($event->users ?? [] as $user)
                                        <div class="col-md-3 col-6 text-center user-{{ $user->id }}">
                                            <div class="avatar avatar-xl">
                                                <img class='img-fulid rounded-circle oraginzer-img' src="{{ $user->profileImage() }}"
                                                    alt="{{ $user->fullName() }}">
                                                {{-- <span class="avatar-status-online"></span> --}}
                                            </div>
                                            <p class="text-center">
                                                {{ $user->fullName() }}
                                                <small class='d-block'>
                                                    {{ $user->hasRole('admin') ? 'Admin' : 'Presenter' }}
                                                </small>
                                                <small class='text-danger user-status d-block mb-50'>Offline</small>
                                                @if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('admin'))
                                                    <button data-user="{{ $user->id }}"
                                                        class="btn  btn-block d-block btn-outline-danger enter-stream btn-sm">Add to Stream</button>
                                                @endif
                                            </p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('admin') )
                            <div class="row d-none">
                                <div class="col-12">
                                    <button id="startRecording" class="btn btn-outline-success" disabled=true>Start Recording</button>
                                    <button id="stopRecording" class="btn btn-outline-danger" disabled=true>Stop Recording</button>
                                </div>
                            </div>
                        @endif
                        <h4>Notes</h4>
                        @if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('admin'))
                            <ul>
                                <li>Once you click "Go Live", video feed of active user will start showing on the website.</li>
                                <li>You can add Presenters to the stream before going live to make sure evertyhing is good before publishing the event to end users.</li>

                            </ul>
                        @else
                            <ul>
                                <li>You will get access to present yourself once access is given by event admin to you.</li>
                                <li>You can check your Camera & Microphone devices in advance.</li>
                                <li>You will see text "You are presenting now" once you get access from Event Admin.</li>
                            </ul>
                        @endif
                    </div>
                    <!-- general tab -->
                    <div role="tabpanel" class="tab-pane border" id="chat-box" aria-labelledby="chat-box" aria-expanded="true">
    
                        <!-- Main chat area -->
                        <section class="chat-app-window">
                            <!-- To load Conversation -->
                            <div class="start-chat-area d-none">
                                <div class="mb-1 start-chat-icon">
                                    <i data-feather="message-square"></i>
                                </div>
                                <h4 class="sidebar-toggle start-chat-text">Start Conversation</h4>
                            </div>
                            <!--/ To load Conversation -->
    
                            <!-- Active Chat -->
                            <div class="active-chat">
                                <!-- Chat Header -->
                                <div class="chat-navbar">
                                    <header class="chat-header">
                                        <div class="d-flex align-items-center">
                                            {{-- <div class="sidebar-toggle d-block d-lg-none mr-1">
                                                <i data-feather="menu" class="font-medium-5"></i>
                                            </div> --}}
    
                                            <h4 class="mb-0">Public Chat</h6>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            @if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('admin'))
                                                <button class='btn btn-outline-primary toggle-chat  ' data-val="off">Enable
                                                    Chat</button>
                                            @endif
                                        </div>
                                    </header>
                                </div>
                                <!--/ Chat Header -->
    
                                <!-- User Chat messages -->
                                <div class="user-chats" style="height: 70vh;">
                                    <div class="chats chat-message-box">
    
                                    </div>
                                </div>
                                <!-- User Chat messages -->
    
                                <!-- Submit Chat form -->
                                <form class="chat-app-form" action="javascript:void(0);" onsubmit="enterChat();">
                                    <div class='chat-disabled text-center w-100 d-none'>
                                        <h4 class="mb-0"> Chat disabled by Event Organizer.
                                        </h4>
                                    </div>
                                    <div class="input-group chat-element d-none input-group-merge mr-1 form-send-message">
                                        <input type="text" class="form-control message" placeholder="Type your message..." />
                                    </div>
                                    <button type="button" class="btn chat-element d-none btn-primary send"
                                        onclick="enterChat();">
                                        <i data-feather="send" class="d-lg-none"></i>
                                        <span class="d-none d-lg-block">Send</span>
                                    </button>
                                </form>
                                <!--/ Submit Chat form -->
                            </div>
                            <!--/ Active Chat -->
                        </section>
                        <!--/ Main chat area -->
    
    
                    </div>

                    <div role="tabpanel" class="tab-pane d-none" id="notes-box" aria-labelledby="notes-box" aria-expanded="true">
                        @if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('admin'))
                            <p>
                                <strong>Note:</strong> This message will only visible to Event Admin.
                            </p>
                            <ul>
                                <li>Once you click "Go Live", video feed of active user will start showing on the website.</li>
                                <li>You can add Presenters to the stream before going live to make sure evertyhing is good before publishing the event to end users.</li>

                            </ul>
                        @else
                            <ul>
                                <li>You will get access to present yourself once access is given by event admin.</li>
                                <li>You can check your Camera & Microphone devices before live evnet.</li>
                                <li>You will see text "You are presenting now" once you get access from Event Admin.</li>
                            </ul>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="media-device-test" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-label">Media Device Test</h5>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row mb-2">
                            <div class="col-12">
                                <h5 class="device-name">Camera</h5>
                                <p id="device-intro">Move in front of the camera to check if it works.</p>
                                <div class="input-group mb-50">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-outline-secondary dropdown-toggle" type="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cams</button>
                                        <div class="cam-list dropdown-menu"></div>
                                    </div>
                                    <input type="text" class="cam-input form-control"
                                        aria-label="Text input with dropdown button" readonly>
                                </div>
                                <div id="pre-local-player" style="height: 40vh;" class="player"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h5 class="device-name">Microphone</h5>
                                <p id="device-intro">Produce sounds to check if the mic works.</p>
                                <div class="input-group mb-50">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-outline-secondary dropdown-toggle" type="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mics</button>
                                        <div class="mic-list dropdown-menu"></div>
                                    </div>
                                    <input type="text" class="mic-input form-control"
                                        aria-label="Text input with dropdown button" readonly>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="0"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary finish-live-test" data-dismiss="modal">Finish</button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="event-id" value="{{ $event->id }}" />
    <input type="hidden" id="channel-name" value="{{ $event->channel_name }}" />
    <input type="hidden" id="agora-token" value="{{ $event->token }}" />
    <input type="hidden" id="user-id" value="{{ auth()->user()->id }}" />
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    @include('inc/sweet-alert/scripts')
    <script src="https://download.agora.io/sdk/release/AgoraRTC_N-4.4.0.js"></script>
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.4.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.4.2/firebase-database.js"></script>
    <script>
    </script>
@endsection



@section('page-script')
    <script src="{{ asset(mix('js/scripts/pages/app-chat.js')) }}"></script>
    <script src="{{ asset('js/scripts/live.js') }}"></script>
@endsection
